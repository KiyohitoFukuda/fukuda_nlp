import java.util.ArrayList;

import edu.cmu.lti.jawjaw.pobj.POS;
import thesaurus.JapaneseWordNet;

public class Test {

	public static void main(String[] args) {
		JapaneseWordNet jwn = new JapaneseWordNet();
		String s3 = "人間";
		ArrayList<String> l1 = jwn.findSynonyms(s3, POS.n, "名詞-");
		for ( int i = 0; i < l1.size(); i++ ){
			System.out.println(l1.get(i));
		}
		ArrayList<String> l2 = jwn.findHypernyms(s3, POS.n, "名詞-");
		for ( int i = 0; i < l2.size(); i++ ){
			System.out.println(l2.get(i));
		}
		ArrayList<String> l3 = jwn.findHyponyms(s3, POS.n, "名詞-");
		for ( int i = 0; i < l3.size(); i++ ){
			System.out.println(l3.get(i));
		}

	}

}
