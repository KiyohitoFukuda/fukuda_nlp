package crawling;

import java.util.ArrayList;

import data.NovelData;

public class MainAozora {

	public static void main(String[] args){
		try{
			String[] writerCodes =				// 作家コード
				{"000879",		// 芥川龍之介
				 "000035",		// 太宰治
				 "000009",		// ドイルアーサーコナン
				 "000081",		// 宮沢賢治
				 "000148"};		// 夏目漱石
			String[][] novelCodes =			// 作品コード
				{{"69_14933", "127_15260", "42_15228"},
				 {"1565_8559", "301_14912", "1567_14913"},
				 {"45340_18750", "43523_17368", "43497_16596"},
				 {"462_15405", "1924_14254", "43737_19215"},
				 {"794_14946", "789_14547", "752_14964"}};
			WebCrawler wc = new WebCrawler();
			ArrayList<String> nList = new ArrayList<String>();
			ArrayList<String> wList = new ArrayList<String>();
			for ( int i = 0; i < writerCodes.length; i++){
				for ( int j = 0; j < novelCodes[i].length; j++){
					nList.add(novelCodes[i][j]);
					wList.add(writerCodes[i]);
				}
			}
		// 作業開始
			for ( int i = 0; i < nList.size(); i++){
			// クローリング
				ArrayList<String> novel = new ArrayList<String>();
				novel = wc.crawlingAozoraNovels(wList.get(i), nList.get(i));
				System.out.println("クローリング終了 : " + nList.get(i));
			// データ生成
				NovelData nd = new NovelData(nList.get(i), wList.get(i));
				nd.setNovel(novel);
				nd.separateNovel(true);
				System.out.println("データ生成終了 : " + nList.get(i));
			}


		} catch ( Exception e){
			e.printStackTrace();
		}
	}
}
