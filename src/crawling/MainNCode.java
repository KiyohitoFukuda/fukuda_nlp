package crawling;

import java.util.ArrayList;

import data.NovelData;
import data.NovelInfo;

public class MainNCode {

	public static void main(String[] args){
		try{
/*
			String[]   genre =			// ジャンル
				{"Fantasy",
				 "Love",
				 "Mystery",
				 "SF",
				 "School"};
			String[][] nCodes =		// 小説 {ファンタジー, 恋愛, 推理, SF, 学園}
				{{"n9669bk", "n4237cd", "n3963ce", "n5881cl", "n1337cn", "n3420cm", "n2027ci", "n6990ch", "n4202cb", "n6266ci"},
				 {"n4499cf", "n4029bs", "n5040ce", "n7995ce", "n3059ch", "n7855ck", "n8388cp", "n5715cp", "n4484cq", "n7012ce"},
				 {"n9636x", "n9729ch", "n8899ce", "n7033br", "n2166bu", "n2810cc", "n9440cl", "n6086bu", "n4376bl", "n5954ca"},
				 {"n4527bc", "n5011bc", "n1930bw", "n5834cr", "n4745cf", "n8335bk", "n9463br", "n5084bv", "n8266cp", "n5714cl"},
				 {"n0595ch", "n3488cf", "n8753cq", "n1466cj", "n5055cf", "n5275cr", "n6175cl", "n8260ce", "n1481ce","n5945w"}};

			WebCrawler wc = new WebCrawler();
			ArrayList<String> nList = new ArrayList<String>();
			ArrayList<String> gList = new ArrayList<String>();
			for ( int i = 0; i < genre.length; i++){
				for ( int j = 0; j < nCodes[i].length; j++){
					nList.add(nCodes[i][j]);
					gList.add(genre[i]);
				}
			}
		// 作業開始
			for ( int i = 0; i < nList.size(); i++){
			// クローリング
				ArrayList<String> novel = new ArrayList<String>();
				novel = wc.crawlingNCodeNovels(nList.get(i));
				System.out.println("クローリング終了 : " + nList.get(i));
			// データ生成
				NovelData nd = new NovelData(nList.get(i), gList.get(i));
				nd.setNovel(novel);
				nd.separateNovel();
				System.out.println("データ生成終了 : " + nList.get(i));
			}
*/

			Search s = new Search();
			YomouCrawler yc = new YomouCrawler();

		// 前回エラーが出た小説のクローリング
			ArrayList<String[]> error = yc.loadErrorList();
			System.out.println("前回のエラー : " + error.size() + " 件");
			System.out.println();
			for ( int i = 0; i < error.size(); i++ ){
				String[] eInfo = error.get(i);
				NovelInfo info = yc.getNovelInfo(eInfo[2], eInfo[0]);
				if ( info.checkUpdate() ){
					ArrayList<String> novel = new ArrayList<String>();
					novel = yc.crawlNovel(info.getURL());
				// エラーチェック
				// エラーでなければデータ生成・保存
					if ( novel.size() > 0){
						NovelData nd = new NovelData(info.getNCode(), info.getGenre());
						nd.setNovel(novel);
						nd.separateNovel(false);
						info.saveNovelInfo();
						System.out.println("データ生成終了 : " + info.getNCode());
					}
				// エラーならばエラーリストを保存後, 次の小説のクローリングへ
					else{
						yc.saveList();
						Thread.sleep(60000);
					}
				}
				Thread.sleep(1000);
			}


		// 通常のクローリング
		// ジャンル取得
			ArrayList<String> allType = s.searchType();
			for ( int i = 0; i < allType.size(); i++ ){
				String[] type = allType.get(i).split(",");
			// ジャンルごとのランキング取得
				ArrayList<String> ranked = s.searchRankingInfo(type[2]);
				for ( int j = 0; j < ranked.size(); j++ ){
				// 小説情報および小説本文のクローリング
					System.out.println();
					System.out.println("タイプ : " + type[1] + "\t\tランク : " + (j+1) + " 位");
					String    rank       = ranked.get(j);
					NovelInfo info = yc.getNovelInfo(rank, type[0]);
					if ( info != null ){
						if ( info.checkUpdate() ){
							ArrayList<String> novel = new ArrayList<String>();
							novel = yc.crawlNovel(info.getURL());
						// エラーチェック
						// エラーでなければデータ生成・保存
							if ( novel.size() > 0){
								NovelData nd = new NovelData(info.getNCode(), info.getGenre());
								nd.setNovel(novel);
								nd.separateNovel(false);
								info.saveNovelInfo();
								System.out.println("データ生成終了 : " + info.getNCode());
							}
						// エラーならばエラーリストを保存後, 次の小説のクローリングへ
							else{
								yc.saveList();
								Thread.sleep(60000);
							}
						}else{
							info.saveNovelInfo();
						}
					}
					long time = (long)(1000 * 1.2);
					Thread.sleep(time);
				}
			}

		} catch ( Exception e){
			e.printStackTrace();
		}
	}
}
