package crawling;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;

public class Search {


/**
 * コンストラクタ
 */
	public Search(){

	}





	public ArrayList<String> searchRankingInfo(String type){
		try{
		// ランキング入りした小説情報の URL
			ArrayList<String> novelInfo = new ArrayList<String>();


		// 種類ごとのランキングを獲得する URL を指定
			String urlHead = "http://yomou.syosetu.com";
			URL    url     = new URL(urlHead + type);

		// 獲得開始
			InputStreamReader isr = new InputStreamReader(url.openStream(), "utf-8");
			BufferedReader    br  = new BufferedReader(isr);
			String line = null;
			while ( (line=br.readLine()) != null ){
				if ( line.contains("a target") ){
					String rankedUrl = line.split("\"")[3];
					novelInfo.add(rankedUrl);
				}
			}
			br.close();

			return novelInfo;
		}

		catch ( Exception e){
			e.printStackTrace();
			return null;
		}
	}






	public ArrayList<String> searchType(){
		try{
		// 種類ごとのランキングの URL
			ArrayList<String> allType = new ArrayList<String>();
			ArrayList<String> daily   = new ArrayList<String>();
			ArrayList<String> weekly  = new ArrayList<String>();
			ArrayList<String> monthly = new ArrayList<String>();
			ArrayList<String> quarter = new ArrayList<String>();
			ArrayList<String> yearly  = new ArrayList<String>();

		// ランキングの種類を獲得する URL を指定
			String urlName = "http://yomou.syosetu.com/rank/genretop/";
			URL    url     = new URL(urlName);

		// 検索開始
			InputStreamReader isr = new InputStreamReader(url.openStream(), "utf-8");
			BufferedReader    br  = new BufferedReader(isr);
			String line = null;
			while ( (line=br.readLine()) != null ){
				if ( line.contains("genreranking_ll") ){
					String typeUrl = line.split("→")[1].split("\"")[1];
					String type1 = typeUrl.split("_")[1].split("/")[0];
					String type2 = typeUrl.split("/")[4];
//					allType.add(type1 + "," + type2 + "," + typeUrl);
					if ( type2.contains("daily") ){
						daily.add(type1 + "," + type2 + "," + typeUrl);
					}
					if ( type2.contains("weekly") ){
						weekly.add(type1 + "," + type2 + "," + typeUrl);
					}
					if ( type2.contains("monthly") ){
						monthly.add(type1 + "," + type2 + "," + typeUrl);
					}
					if ( type2.contains("quarter") ){
						quarter.add(type1 + "," + type2 + "," + typeUrl);
					}
					if ( type2.contains("yearly") ){
						yearly.add(type1 + "," + type2 + "," + typeUrl);
					}
//					System.out.println(type1 + "," + type2 + "," + typeUrl);
				}
			}
			allType.addAll(yearly);
			allType.addAll(quarter);
			allType.addAll(monthly);
			allType.addAll(weekly);
			allType.addAll(daily);
			br.close();

			return allType;
		}

		catch ( Exception e){
			e.printStackTrace();
			return null;
		}
	}
}

