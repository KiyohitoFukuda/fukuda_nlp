package crawling;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

import morphological_analysis.CaboCha;
import morphological_analysis.Mecab;

public class Separator {

	Mecab   mecab_;			// Mecab クラス
	CaboCha cabocha_;		// CaboCha クラス


/**
 * コンストラクタ
 */
	public Separator(){
		this.mecab_   = new Mecab();
		this.cabocha_ = new CaboCha();
	}





/**
 * ファイルに保存された小説を各単位に分割する.
 * @param fileName
 */
	public void separateNovel(String fileName, boolean isAll){
		try{
		// 保存してある小説を取得
			File              file  = new File("novel/" + fileName);
			FileReader        fr    = new FileReader(file);
			BufferedReader    br    = new BufferedReader(fr);
			ArrayList<String> novel = new ArrayList<String>();
			String line;
			while ( (line=br.readLine()) != null){
				novel.add(line);
			}
			br.close();
			fr.close();

		// 文に分割
			ArrayList<String> sentence = new ArrayList<String>();
			for ( int i = 0; i < novel.size(); i++){
				String   str  = novel.get(i);
				String[] s = this.separateSentence(str, "。", isAll);
				for ( int j = 0; j < s.length; j++){
					sentence.add(s[j]);
				}
			}
			System.out.println("文分割終了 : " + fileName);
		// 単語に分割
			ArrayList<String> word     = new ArrayList<String>();
			for ( int i = 0; i < sentence.size(); i++){
				String[] w = this.separateWord(sentence.get(i));
				for ( int j = 0; j < w.length; j++ ){
					word.add(w[j]);
				}
			}
			System.out.println("単語分割終了 : " + fileName);
		// 文を保存
			File           writeFile = new File("sentence/" + fileName);
			FileWriter     fw        = new FileWriter(writeFile, false);
			BufferedWriter bw        = new BufferedWriter(fw);
			for ( int i = 0; i < sentence.size(); i++){
				bw.write(sentence.get(i));
				bw.newLine();
			}
			bw.close();
			fw.close();
		// 単語を保存
			File           writeFile2 = new File("word/" + fileName);
			FileWriter     fw2        = new FileWriter(writeFile2, false);
			BufferedWriter bw2        = new BufferedWriter(fw2);
			for ( int i = 0; i < word.size(); i++){
				bw2.write(word.get(i));
				bw2.newLine();
			}
			bw2.close();
			fw2.close();

		} catch ( Exception e){
			e.printStackTrace();
		}
	}





/**
 * 文字列を文に分割する. isAll が true であれば会話文も文として扱う.
 *
 * @param str
 * @param key
 * @param isAll
 * @return
 */
	public String[] separateSentence(String str, String key, boolean isAll){
		String first = String.valueOf(str.charAt(0));
		String last  = String.valueOf(str.charAt(str.length()-1));
	// 会話文の対応
		if ( !isAll ){
			if ( first.equals("「") && last.equals("」") ){
				String[] ans = new String[1];
				ans[0] = str;
				return ans;
			}
		}
	// 文字列を key で分割
		String[] ans = str.split(key);
		for ( int i = 0; i < ans.length-1; i++){
			ans[i] += key;
		}
		if ( last.equals(key) || last.equals("」") ){
			ans[ans.length-1] += key;
		}

		return ans;
	}





// 分割関数 (単語)
/**
 * 文字列を単語に分割する.
 * @param str
 * @return
 */
	public String[] separateWord(String str){
	// 句読点の処理
		str.replaceAll(",", "、");
		str.replaceAll("\\.", "。");
	// 文字列を形態素解析
		if ( !str.equals(mecab_.toString()) ){
			mecab_.setMecabData(str);
		}
		ArrayList<String> base = mecab_.getLemma();
		ArrayList<String> pos  = mecab_.getPos(true);
		String[] ans = new String[base.size()];
		for ( int i = 0; i < ans.length; i++){
			ans[i] = base.get(i) + pos.get(i);
		}

		return ans;
	}

}
