package crawling;

import java.util.ArrayList;

import data.NovelData;

public class TestWebCrawler {

	public static void main(String[] args){
		try{
//			String[] nCodes = {"n6963w", "n4029bs", "n9636x", "n3806cg", "n1714bg"};
//			String[] nCodes = {"n7930q", "n0860u", "n8486bk"};
			String[] nCodes = {"n7930q"};
			WebCrawler wc = new WebCrawler();

		// クローリングテスト
			for ( int i = 0; i < nCodes.length; i++){
			// クローリング
				ArrayList<String> novel = new ArrayList<String>();
				novel = wc.crawlingNCodeNovels(nCodes[i]);
				System.out.println("クローリング終了 : " + nCodes[i]);
			// データ生成
				NovelData nd = new NovelData(nCodes[i], "Test");
				nd.setNovel(novel);
				nd.separateNovel(true);
				System.out.println("データ生成終了 : " + nCodes[i]);
			}

		} catch ( Exception e){
			e.printStackTrace();
		}
	}
}
