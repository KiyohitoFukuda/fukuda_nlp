package crawling;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WebCrawler {

	private String[] types_ = {"文学", "恋愛", "歴史", "推理", "ファンタジー", "SF",
			"ホラー", "コメディー", "冒険", "学園", "戦記", "童話", "詩", "エッセイ",
			"その他"};
	private String t_ = "&type=re";
	private String o_ = "&order=hyoka";
	private String n_ = "&notnizi=1";
	private String g_;





	// http://yomou.syosetu.com から小説をクローリングする
	public ArrayList<String> crawlingNovels(String type, int num){
		try{
		// 小説が載っている URL リスト
			ArrayList<String>  urlList = new ArrayList<String>();
		// その連載数リスト
			ArrayList<Integer> numList = new ArrayList<Integer>();
		// 小説の文章リスト
			ArrayList<String>  novList = new ArrayList<String>();

		// クローリングする URL を指定
			String urlHead = "http://yomou.syosetu.com/search.php?";
			String urlTail = "&p=1";
			g_ = "&genre=15";
			for ( int i = 0; i < types_.length; i++){
				if ( type.equals(types_[i])){
					g_ = "&genre=" + (i+1);
					break;
				}
			}
			URL url = new URL(urlHead + t_ + o_ + n_ + g_ + urlTail);

		// クローリング開始
		// 小説が載っている URL を取得
			InputStreamReader isr = new InputStreamReader(url.openStream(), "utf-8");
			BufferedReader    br  = new BufferedReader(isr);
			String            line;
			while ((line = br.readLine()) != null) {
				if ( line.contains("<div class=\"novel_h\">")){
					String titleUrl = line.split("href=\"")[1].split("\">")[0];
					urlList.add(titleUrl);
				}
				if ( line.contains("全")){
					int number = Integer.parseInt(line.split("全")[1].split("部分")[0]);
					numList.add(number);
				}
			}
			br.close();

		// HTML から必要な部分を取得
			Pattern pattern1  = Pattern.compile("<rt>.*</rt?>");
			Pattern pattern2  = Pattern.compile("<rp>.*</rp?>");
			Pattern pattern3  = Pattern.compile("<.+?>");
			Pattern pattern4  = Pattern.compile("[　 ]");
			String  novelPart = "<div id=\"novel_honbun\" class=\"novel_view\">";
			for ( int i = 0; i < num; i++){
				boolean isNovel   = false;
				for ( int page = 1; page < numList.get(i)+1; page++){
					url = new URL(urlList.get(i) + String.valueOf(page) + "/");
					isr = new InputStreamReader(url.openStream(), "utf-8");
					br  = new BufferedReader(isr);
					while ((line = br.readLine()) != null) {
//						System.out.println(page);
						if ( isNovel){
							if ( line.equals("<br />")){
								continue;
							}
							if ( line.contains("</div>")){
								isNovel = false;
								if ( line.equals("</div>")){
									continue;
								}
							}
							Matcher m = pattern1.matcher(line);
							String  s = m.replaceAll("");
							m = pattern2.matcher(s);
							s = m.replaceAll("");
							m = pattern3.matcher(s);
							s = m.replaceAll("");
							m = pattern4.matcher(s);
							s = m.replaceAll("");
							if ( !s.equals("")){
								novList.add(s);
							}
						}
						if ( line.contains(novelPart)){
							isNovel = true;
							try{
								Matcher m = pattern1.matcher(line);
								String  s = m.replaceAll("");
								m = pattern2.matcher(s);
								s = m.replaceAll("");
								m = pattern3.matcher(s);
								s = m.replaceAll("");
								m = pattern4.matcher(s);
								s = m.replaceAll("");
								if ( !s.equals("")){
									novList.add(s);
								}
							} catch ( Exception e){
								System.out.println(line);
							}
						}
					}
					br.close();
				}
			}

			return novList;

		} catch ( Exception e){
			e.printStackTrace();
			return null;
		}

	}







	public ArrayList<String> crawlingNCodeNovels(String nCode){
		try{
		// 小説が載っている URL リスト
			ArrayList<String>  urlList = new ArrayList<String>();
		// その連載数リスト
			ArrayList<Integer> numList = new ArrayList<Integer>();
		// 小説の文章リスト
			ArrayList<String>  novList = new ArrayList<String>();

		// クローリングする URL を指定
			String urlHead = "http://ncode.syosetu.com/";
			URL    url     = new URL(urlHead + nCode + "/");

		// クローリング開始
		// 小説が載っている URL を取得
			InputStreamReader isr = new InputStreamReader(url.openStream(), "utf-8");
			BufferedReader    br  = new BufferedReader(isr);
			String            line;
			while ((line = br.readLine()) != null) {
				if ( line.contains("<dd class=\"subtitle\">")){
					String titleUrl = line.split("href=\"")[1].split("\">")[0];
					urlList.add(titleUrl);
				}
			}
			br.close();

		// HTML から必要な部分を取得
			Pattern pattern1  = Pattern.compile("<rt>.*?</rt>");
			Pattern pattern2  = Pattern.compile("<rp>.*?</rp>");
			Pattern pattern3  = Pattern.compile("<ruby>.*?</ruby>");
			Pattern pattern4  = Pattern.compile("<.+?>");
			Pattern pattern5  = Pattern.compile("[　 ]");
			String  novelPart = "<div id=\"novel_honbun\" class=\"novel_view\">";
			Matcher m;
			String  s;
			boolean isNovel   = false;
			for ( int i =0 ; i < urlList.size() ; i++){
				url = new URL(urlHead + urlList.get(i));
				isr = new InputStreamReader(url.openStream(), "utf-8");
				br  = new BufferedReader(isr);
				while ((line = br.readLine()) != null) {
					if ( isNovel){
						if ( line.equals("<br />")){
							continue;
						}
						if ( line.contains("</div>")){
							isNovel = false;
						}
						s = line;
//						System.out.println("b : " + s);
						m = pattern1.matcher(s);
						s = m.replaceAll("");
						m = pattern2.matcher(s);
						s = m.replaceAll("");
//						m = pattern3.matcher(s);
//						s = m.replaceAll("");
						m = pattern4.matcher(s);
						s = m.replaceAll("");
						m = pattern5.matcher(s);
						s = m.replaceAll("");
//						System.out.println("a : " + s);
						if ( !s.equals("")){
							novList.add(s);
						}
					}
					if ( line.contains(novelPart)){
						isNovel = true;
						try{
							s = line;
//							System.out.println("b : " + s);
							m = pattern1.matcher(s);
							s = m.replaceAll("");
							m = pattern2.matcher(s);
							s = m.replaceAll("");
//							m = pattern3.matcher(s);
//							s = m.replaceAll("");
							m = pattern4.matcher(s);
							s = m.replaceAll("");
							m = pattern5.matcher(s);
							s = m.replaceAll("");
//							System.out.println("a : " + s);
							if ( !s.equals("")){
								novList.add(s);
							}
						} catch ( Exception e){
							System.out.println(line);
						}
					}
				}
				br.close();
				if ( i == 0 || i == urlList.size()-1){
					System.out.println("crawled " + url.toString());
				}
				if ( (i+1)%100 == 0){
					System.out.println("crawled " + url.toString());
				}
			}

			return novList;

		} catch ( Exception e){
			e.printStackTrace();
			return null;
		}

	}







	public ArrayList<String> crawlingAozoraNovels(String sakkaNum, String sakuhinNum){
		try{
		// 小説が載っている URL リスト
			ArrayList<String>  urlList = new ArrayList<String>();
		// その連載数リスト
			ArrayList<Integer> numList = new ArrayList<Integer>();
		// 小説の文章リスト
			ArrayList<String>  novList = new ArrayList<String>();

		// クローリングする URL を指定
			String urlHead = "http://www.aozora.gr.jp/cards/";
			URL    url     = new URL(urlHead + sakkaNum + "/files/" + sakuhinNum + ".html");

		// クローリング開始
		// 小説が載っている URL を取得
			String  line;

		// HTML から必要な部分を取得
			Pattern pattern1  = Pattern.compile("<rt>.*?</rt>");
			Pattern pattern2  = Pattern.compile("<rp>.*?</rp>");
			Pattern pattern3  = Pattern.compile("<ruby>.*?</ruby>");
			Pattern pattern4  = Pattern.compile("<.+?>");
			Pattern pattern5  = Pattern.compile("［.+?］");
			String  novelPart = "<div class=\"main_text\">";
			Matcher m;
			String  s;
			boolean isNovel   = false;

			InputStreamReader isr = new InputStreamReader(url.openStream(), "MS932");
			BufferedReader    br  = new BufferedReader(isr);
			while ((line = br.readLine()) != null) {
				if ( isNovel){
					if ( line.contains("<div class=\"bibliographical_information\">")){
						isNovel = false;
						continue;
					}
					if ( line.equals("<br />")){
						continue;
					}
					if ( line.contains("div")){
						continue;
					}
					s = line;
//					System.out.println("b : " + s);
					m = pattern1.matcher(s);
					s = m.replaceAll("");
					m = pattern2.matcher(s);
					s = m.replaceAll("");
//					m = pattern3.matcher(s);
//					s = m.replaceAll("");
					m = pattern4.matcher(s);
					s = m.replaceAll("");
					m = pattern5.matcher(s);
					s = m.replaceAll("");
//					System.out.println("a : " + s);
					if ( !s.equals("")){
						novList.add(s);
					}
				}
				if ( line.contains(novelPart)){
					isNovel = true;
					try{
						s = line;
//						System.out.println("b : " + s);
						m = pattern1.matcher(s);
						s = m.replaceAll("");
						m = pattern2.matcher(s);
						s = m.replaceAll("");
//						m = pattern3.matcher(s);
//						s = m.replaceAll("");
						m = pattern4.matcher(s);
						s = m.replaceAll("");
						m = pattern5.matcher(s);
						s = m.replaceAll("");
//						System.out.println("a : " + s);
						if ( !s.equals("")){
							novList.add(s);
						}
					} catch ( Exception e){
						System.out.println(line);
					}
				}
			}
			br.close();

			System.out.println("crawled " + url.toString());

			return novList;

		} catch ( Exception e){
			e.printStackTrace();
			return null;
		}

	}

}
