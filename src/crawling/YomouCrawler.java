package crawling;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.nio.file.LinkOption;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import data.NovelInfo;

public class YomouCrawler {

	private ArrayList<String> successList_;
	private ArrayList<String> errorList_;
	private ArrayList<String> novel_;

	private String work_;			// ジャンル, N コード,小説情報の URL

	private boolean isNovel_;			// 小説本文が掲載されている部分かどうか
	private boolean isShort_;			// 短編であるかどうか


/**
 * コンストラクタ
 */
	public YomouCrawler(){
		this.isNovel_ = false;
		this.isShort_ = false;

		this.successList_ = new ArrayList<String>();
		this.errorList_   = new ArrayList<String>();
	}





	public NovelInfo getNovelInfo(String rankedInfo, String genre){
		try{
		// 小説情報の URL を指定
			URL url = new URL(rankedInfo);
		// 獲得開始
			InputStreamReader isr = new InputStreamReader(url.openStream(), "utf-8");
			BufferedReader    br  = new BufferedReader(isr);
			String line    = "";
			String code    = "";
			String title   = "";
			String urlName = "";
			String author  = "";
			String keyword = "";
			String update  = "";
			String type    = "";
			String wordNum = "";
			boolean isTitle   = false;
			boolean isAuthor  = false;
			boolean isKeyword = false;
			boolean isUpdate1 = false;
			boolean isUpdate2 = false;
			boolean isType    = false;
			boolean isNumber  = false;
			while ( (line=br.readLine()) != null ){
				if ( line.contains("Nコード") ){
					isTitle = true;
					continue;
				}
				if ( line.contains("作者名<") ){
					isAuthor = true;
					continue;
				}
				if ( line.contains("キーワード<") ){
					isKeyword = true;
					continue;
				}
				if ( line.contains("掲載日<") ){
					isUpdate1 = true;
					continue;
				}
				if ( line.contains("更新日<") ){
					isUpdate2 = true;
					continue;
				}
				if ( line.contains("noveltype") ){
					isType = true;
				}
				if ( line.contains("文字数<") ){
					isNumber = true;
					continue;
				}

				if ( isTitle ){
					Pattern  p = Pattern.compile("<.+?>");
					Matcher  m = p.matcher(line);
					System.out.println(line);
					urlName = line.split("\"")[1];
					title   = m.replaceAll(",").split(",")[2];
					code    = urlName.split("/")[3];
					isTitle = false;
					continue;
				}
				if ( isAuthor ){
					Pattern  p = Pattern.compile("<.+?>");
					Matcher  m = p.matcher(line);
					String[] data = m.replaceAll(",").split(",");
					author = data[data.length-1];
					isAuthor = false;
					continue;
				}
				if ( isKeyword ){
					Pattern  p    = Pattern.compile("<.+?>");
					Matcher  m    = p.matcher(line);
					String   data = m.replaceAll("");
					if ( !data.equals("") ){
						keyword += data;
					}
					if ( line.equals("</td>") ){
						keyword = keyword.replace("&nbsp;", ",");
						keyword = keyword.replace(" ", ",");
						isKeyword = false;
					}
					continue;
				}
				if ( isUpdate1 ){
					Pattern  p = Pattern.compile("<.+?>");
					Matcher  m = p.matcher(line);
					update = m.replaceAll(",").split(",")[1];
					isUpdate1 = false;
					continue;
				}
				if ( isUpdate2 ){
					String[] data = line.split(" ");
					if (data.length > 1 ){
						update = line;
						isUpdate2 = false;
					}
					continue;
				}
				if ( isType ){
					Pattern  p = Pattern.compile("<.+?>");
					Matcher  m = p.matcher(line);
					String[] data = m.replaceAll(",").split(",");
					type = data[1];
					for ( int i = 2; i < data.length; i++ ){
						type += "-" + data[i];
					}
					isType = false;
					continue;
				}
				if ( isNumber ){
					Pattern  p = Pattern.compile("<.+?>");
					Matcher  m = p.matcher(line);
					wordNum = m.replaceAll("");
					isNumber = false;
					continue;
				}
			}
			br.close();
			isr.close();

		// テスト
			System.out.println("ジャンル : " + genre);
			System.out.println("N コード : " + code);
			System.out.println("作品名 : " + title);
			System.out.println("URL : " + urlName);
			System.out.println("作者 : " + author);
			System.out.println("キーワード : " + keyword);
			System.out.println("更新日 : " + update);
			System.out.println("小説種別 : " + type);
			System.out.println("文字数 : " + wordNum);
			this.work_ = genre + "," + code + "," + rankedInfo;

			NovelInfo ni = new NovelInfo();
			ni.setNovelInfo(genre, code, title, urlName, author, keyword, update, type, wordNum);

			return ni;
		}

		catch ( Exception e){
			e.printStackTrace();
			return null;
		}
	}





	public ArrayList<String> crawlNovel(String novelUrl){
		try{
		// 小説の本体 (文章)
			ArrayList<String> novel = new ArrayList<String>();
		// 小説が掲載された URL を発見するための URL を指定
//			System.out.println("url_name : " + novelUrl);
			URL preUrl = new URL(novelUrl);
			ArrayList<String> urlList = new ArrayList<String>();
		// 検索開始
			InputStreamReader isr1 = new InputStreamReader(preUrl.openStream(), "utf-8");
			BufferedReader    br1  = new BufferedReader(isr1);
			String line1  = null;
			int   count = 0;
			while ( (line1=br1.readLine()) != null ){
			// 長編の場合
				if ( line1.contains("subtitle") ){
					count++;
					if ( line1.split("\"")[3].split("/").length > 2 ){
						urlList.add(novelUrl + count + "/");
//						System.out.println(novelUrl + count + "/");
					}
				}
			// 短編の場合
				if ( line1.contains("novel_honbun") ){
//					System.out.println("短編 : 1 話");
					this.isNovel_ = true;
					this.isShort_ = true;
				}
				if ( this.isNovel_ ){
					String s = getNovelSentence(line1);
					if ( s.equals("continue") ){
						continue;
					}
					if ( s.equals("break") ){
						break;
					}
					if ( !s.equals("") ){
//						System.out.println(s);
						novel.add(s);
					}
				}
			}
			br1.close();
			isr1.close();

		// 短編ならここまでに本文が取れるので, もう返す
			if ( this.isShort_ ){
				this.isShort_ = false;
				this.successList_.add(this.work_);

				return novel;
			}


		// 小説の本体を獲得開始
//			System.out.println("長編 : " + urlList.size() + " 話");
			for ( int i = 0; i < urlList.size(); i++ ){
			// 小説が掲載された URL を指定
				URL url = new URL(urlList.get(i));
			// 獲得開始
				InputStreamReader isr2 = new InputStreamReader(url.openStream(), "utf-8");
				BufferedReader    br2  = new BufferedReader(isr2);
				String   line2   = null;
				while ( (line2=br2.readLine()) != null ){
					if ( line2.contains("novel_honbun") ){
						this.isNovel_ = true;
					}
					if ( this.isNovel_ ){
						String s = getNovelSentence(line2);
						if ( s.equals("continue") ){
							continue;
						}
						if ( s.equals("break") ){
							break;
						}
						if ( !s.equals("") ){
//							System.out.println(s);
							novel.add(s);
						}
					}
				}
				br2.close();
				isr2.close();
				long time = (long)(1000 * 1.5);
				Thread.sleep(time);
				if ( i%20 == 0 ){
					System.out.print(" " + i + " ");
				}else if ( i%10 == 0 ){
					System.out.print("……");
				}
			}
			System.out.println(urlList.size());
			this.successList_.add(this.work_);

			return novel;
		}

		catch ( Exception e){
			e.printStackTrace();
			this.errorList_.add(this.work_);

			return new ArrayList<String>();
		}
	}





	private String getNovelSentence(String source){
		Pattern p1 = Pattern.compile("<rt>.+?</rt>");
		Pattern p2 = Pattern.compile("<rp>.+?</rp>");
		Pattern p3 = Pattern.compile("<.+?>");

		String sentence = source;
		if ( sentence.equals("<br />") ){
			sentence = "continue";
			return sentence;
		}
		if ( sentence.contains("</div>") ){
			this.isNovel_ = false;
			sentence = "break";
			return sentence;
		}

		Matcher m1 = p1.matcher(sentence);
		sentence = m1.replaceAll("");
		Matcher m2 = p2.matcher(sentence);
		sentence = m2.replaceAll("");
		Matcher m3 = p3.matcher(sentence);
		sentence = m3.replaceAll("");

		return sentence;
	}





	public void saveList(){
		try{
			String absPath = Paths.get(".").toRealPath(LinkOption.NOFOLLOW_LINKS).toString();
			File directory = new File(absPath + "/INFO/");
			if (!directory.exists()){
				directory.mkdirs();
			}
		// クローリングに成功した一覧
			String sFile = directory + "/successNovel";
			FileOutputStream   sFos = new FileOutputStream(sFile);
			OutputStreamWriter sOsw = new OutputStreamWriter(sFos, "SJIS");
			BufferedWriter     sBw  = new BufferedWriter(sOsw);
			for ( int i = 0; i < this.successList_.size(); i++ ){
				sBw.write(this.successList_.get(i));
				sBw.newLine();
			}
			sBw.close();
			sOsw.close();
			sFos.close();

		// クローリングに失敗した一覧
			String eFile = directory + "/errorNovel";
			FileOutputStream   eFos = new FileOutputStream(eFile);
			OutputStreamWriter eOsw = new OutputStreamWriter(eFos, "SJIS");
			BufferedWriter     eBw  = new BufferedWriter(eOsw);
			for ( int i = 0; i < this.errorList_.size(); i++ ){
				eBw.write(this.errorList_.get(i));
				eBw.newLine();
			}
			eBw.close();
			eOsw.close();
			eFos.close();
		}

		catch ( Exception e){
			e.printStackTrace();
		}
	}





	public ArrayList<String[]> loadErrorList(){
		try{
			ArrayList<String[]> errorInfo = new ArrayList<String[]>();

			String absPath = Paths.get(".").toRealPath(LinkOption.NOFOLLOW_LINKS).toString();
			File errorFile = new File(absPath + "/INFO/errorNovel");
			if ( errorFile.exists() ){
				FileInputStream   fis = new FileInputStream(errorFile);
				InputStreamReader isr = new InputStreamReader(fis, "SJIS");
				BufferedReader    br  = new BufferedReader(isr);
			// 読み込み
				String line = null;
				while ( (line=br.readLine()) != null){
					String[] data = line.split(",");
					errorInfo.add(data);
				}
				br.close();
				isr.close();
				fis.close();
			}

			return errorInfo;
		}

		catch ( Exception e){
			e.printStackTrace();

			return new ArrayList<String[]>();
		}
	}

}








