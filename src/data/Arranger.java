package data;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.file.LinkOption;
import java.nio.file.Paths;

import morphological_analysis.Mecab;

public class Arranger {

	private static final String INPUT_CHAR  = "SJIS";
	private static final String OUTPUT_CHAR = "UTF-8";


	public Arranger(){

	}





	public void mergeNovelForDoc2vec(String[] dirs){
		try{
			String absPath = Paths.get(".").toRealPath(LinkOption.NOFOLLOW_LINKS).toString();

			String fileNameData = absPath + "/NOVEL/wakati_" + dirs[0] + "-" + dirs[dirs.length-1];
			FileOutputStream   fosData = new FileOutputStream(fileNameData);
			OutputStreamWriter oswData = new OutputStreamWriter(fosData, OUTPUT_CHAR);
			BufferedWriter     bwData  = new BufferedWriter(oswData);
			String fileNameLabel = absPath + "/NOVEL/label_" + dirs[0] + "-" + dirs[dirs.length-1];
			FileOutputStream   fosLabel = new FileOutputStream(fileNameLabel);
			OutputStreamWriter oswLabel = new OutputStreamWriter(fosLabel, OUTPUT_CHAR);
			BufferedWriter     bwLabel  = new BufferedWriter(oswLabel);

			Mecab mecab = new Mecab();
			for ( int i = 0; i < dirs.length; i++ ){
				File directory = new File(absPath + "/NOVEL/" + dirs[i]);
				String[] fileNames = directory.list();
				for ( int j = 0; j < fileNames.length; j++ ){
					String fileName = directory + "/" + fileNames[j];
					FileInputStream   fis = new FileInputStream(fileName);
					InputStreamReader isr = new InputStreamReader(fis, INPUT_CHAR);
					BufferedReader    br  = new BufferedReader(isr);
					String line  = null;
					int    count = 0;
					while ( (line=br.readLine()) != null ){
						if ( count%500 == 0 ){
							System.out.println(dirs[i] + "--" + fileNames[j] + "--" + count);
						}
						mecab.setMecabData(line);
						String label = dirs[i] + String.format("%08d", count);
						bwData.write(mecab.getWakati());
						bwData.newLine();
						bwLabel.write(label);
						bwLabel.newLine();
						count++;
					}
					System.out.println();
					br.close();
					isr.close();
					fis.close();
				}
			}
			bwData.close();
			oswData.close();
			fosData.close();
			bwLabel.close();
			oswLabel.close();
			fosLabel.close();
		}

		catch ( Exception e){
			e.printStackTrace();
		}

	}
}
