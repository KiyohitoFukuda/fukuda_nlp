package data;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.file.LinkOption;
import java.nio.file.Paths;
import java.util.ArrayList;

public class NovelData {

	private String work_;		// 作品名 (作品コード)
	private String genre_;		// ジャンルコード (作家コード)

	private ArrayList<String> novel_;		// 小説データ

	private Sentence sentence_;		// 文情報
	private Word     word_;			// 単語情報



/**
 * コンストラクタ
 */
	public NovelData(String work, String genre){
		this.work_  = work;
		this.genre_ = genre;

		this.novel_    = new ArrayList<String>();
		this.sentence_ = new Sentence();
		this.word_     = new Word();
	}





/**
 * 設定を初期化する.
 * @param work
 * @param genre
 */
	public void init(String work, String genre){
		this.work_  = work;
		this.genre_ = genre;

		this.novel_    = new ArrayList<String>();
		this.sentence_ = new Sentence();
		this.word_     = new Word();
	}





/**
 * 小説を各単位に分割する.
 */
	public void separateNovel(boolean isDeep){
	// 文に分割・文情報の取得
		for ( int i = 0; i < this.novel_.size(); i++){
			this.sentence_.makeSentence( this.novel_.get(i), "。");
		}
		this.sentence_.saveSentence(this.work_, this.genre_);
	// 単語に分割・文情報の取得 (引数が true の場合のみ)
		if ( isDeep ){
			for ( int i = 0; i < this.sentence_.getSize(); i++){
				this.word_.makeWordInfo(this.sentence_.getSentence(i));
			}
			this.word_.saveWordInfo(this.work_, this.genre_);
		}
	}





/**
 * ファイルから小説データを読み込む
 */
	public void loadNovel(){
		try{
		// 読み込むファイル
			String file = "NOVEL/" + this.genre_ + "/" + this.work_;
			FileInputStream   fis = new FileInputStream(file);
			InputStreamReader isr = new InputStreamReader(fis, "SJIS");
			BufferedReader    br  = new BufferedReader(isr);
		// 読み込み
			String line = null;
			while ( (line=br.readLine()) != null){
				this.novel_.add(line);
			}
			br.close();
			isr.close();
			System.out.println("load sentence : " + this.work_);

		} catch ( Exception e){
			e.printStackTrace();
		}
	}





/**
 * ファイルに小説データを書き込む
 */
	public void saveNovel(){
		try{
		// ディレクトリの確認
			String absPath = Paths.get(".").toRealPath(LinkOption.NOFOLLOW_LINKS).toString();
			File directory = new File(absPath + "/NOVEL/" + this.genre_);
			if ( !directory.exists()){
				directory.mkdirs();
			}
		// 書き込むファイル
			FileOutputStream   fos = new FileOutputStream(directory + "/" + this.work_);
			OutputStreamWriter osw = new OutputStreamWriter(fos, "SJIS");
			BufferedWriter     bw  = new BufferedWriter(osw);
		// 書き込み
			for ( int k = 0; k < this.novel_.size(); k++){
				bw.write(this.novel_.get(k));
				bw.newLine();
			}
			bw.close();
			osw.close();
			System.out.println("sava novel : " + this.work_);

		} catch ( Exception e){
			e.printStackTrace();
		}
	}





/**
 * 単語情報を返す.
 *
 * @return
 */
	public Word getWordInfomation(){
		return this.word_;
	}





/**
 * 文情報を返す.
 * @return
 */
	public Sentence setSentenceInformation(){
		return this.sentence_;
	}





/**
 * 小説を設定する.
 *
 * @param novel
 */
	public void setNovel(ArrayList<String> novel){
		for ( int i = 0; i < novel.size(); i++ ){
			this.novel_.add(novel.get(i));
		}
		saveNovel();
	}

}







