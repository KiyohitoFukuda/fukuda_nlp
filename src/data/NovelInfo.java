package data;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.file.LinkOption;
import java.nio.file.Paths;

public class NovelInfo {

	private final String NO_DATA = "no_data";

	private String genre_;
	private String code_;
	private String url_;
	private String title_;
	private String author_;
	private String keyword_;
	private String update_;
	private String type_;
	private String wordNum_;

	private boolean isUpdate_;


/**
 * コンストラクタ
 */
	public NovelInfo(){
		this.genre_   = this.NO_DATA;
		this.code_    = this.NO_DATA;
		this.url_     = this.NO_DATA;
		this.title_   = this.NO_DATA;
		this.author_  = this.NO_DATA;
		this.keyword_ = this.NO_DATA;
		this.update_  = this.NO_DATA;
		this.type_    = this.NO_DATA;
		this.wordNum_ = this.NO_DATA;

		this.isUpdate_ = false;
	}





// 小説情報からその小説をクローリングする必要があるかをチェックする.
	public void setNovelInfo(String genre, String work, String title, String url, String author,
			String keyword, String update, String type, String wordNum){



		// 以前クローリングした小説情報の読み込み
		loadNovelInfo(genre, work);
	// 以前に一度もクローリングしたことがない場合
		if ( this.title_.equals(this.NO_DATA) ){
			this.genre_   = genre;
			this.code_    = work;
			this.url_     = url;
			this.title_   = title;
			this.author_  = author;
			this.keyword_ = keyword;
			this.update_  = update;
			this.type_    = type;
			this.wordNum_ = wordNum;
			this.isUpdate_  = true;
		}
	// 更新日時が変更されている場合
		if ( !this.update_.equals(update) ){
			this.genre_   = genre;
			this.code_    = work;
			this.url_     = url;
			this.title_   = title;
			this.author_  = author;
			this.keyword_ = keyword;
			this.update_  = update;
			this.type_    = type;
			this.wordNum_ = wordNum;
			this.isUpdate_  = true;
		}
	}





	public void loadNovelInfo(String genre, String work){
		try{
		// 読み込むファイルが存在すれば, 読み込む
			String fileName = "INFO/" + genre + "/" + work;
			File file = new File(fileName);
			if ( file.exists() ){
				FileInputStream   fis = new FileInputStream(fileName);
				InputStreamReader isr = new InputStreamReader(fis, "SJIS");
				BufferedReader    br  = new BufferedReader(isr);
			// 読み込み
				String line = null;
				while ( (line=br.readLine()) != null){
					String[] data = line.split(",");
					if ( data[0].equals("ジャンル") ){
						this.genre_ = data[1];
					}
					if ( data[0].equals("Nコード") ){
						this.code_ = data[1];
					}
					if ( data[0].equals("作品名") ){
						this.title_ = data[1];
					}
					if ( data[0].equals("URL") ){
						this.url_ = data[1];
					}
					if ( data[0].equals("作者") ){
						this.author_ = data[1];
					}
					if ( data[0].equals("キーワード") ){
						this.keyword_ = data[1];
					}
					if ( data[0].equals("更新日時") ){
						this.update_ = data[1];
					}
				}
				br.close();
				isr.close();
				fis.close();
				System.out.println("load novel information : " + work);
			}
		}

		catch ( Exception e){
			e.printStackTrace();
		}
	}





	public void saveNovelInfo(){
		try{
		// ディレクトリの確認
			String absPath = Paths.get(".").toRealPath(LinkOption.NOFOLLOW_LINKS).toString();
			File directory = new File(absPath + "/INFO/" + this.genre_);
			if (!directory.exists()){
				directory.mkdirs();
			}
		// 書き込むファイル
			String file = directory + "/" + this.code_;
			FileOutputStream   fos = new FileOutputStream(file);
			OutputStreamWriter osw = new OutputStreamWriter(fos, "SJIS");
			BufferedWriter     bw  = new BufferedWriter(osw);
		// 書き込み
			bw.write("ジャンル," + this.genre_);
			bw.newLine();
			bw.write("Nコード," + this.code_);
			bw.newLine();
			bw.write("作品名," + this.title_);
			bw.newLine();
			bw.write("URL," + this.url_);
			bw.newLine();
			bw.write("作者," + this.author_);
			bw.newLine();
			bw.write("キーワード," + this.keyword_);
			bw.newLine();
			bw.write("更新日時," + this.update_);
			bw.newLine();
			bw.write("小説種別," + this.type_);
			bw.newLine();
			bw.write("文字数," + this.wordNum_);
			bw.newLine();
			bw.close();
			osw.close();
			fos.close();

			System.out.println("save novel information : " + this.code_);

		} catch ( Exception e){
			e.printStackTrace();
		}
	}





	public boolean checkUpdate(){
		return this.isUpdate_;
	}





// ゲッター関数

	public String getGenre(){
		return this.genre_;
	}



	public String getNCode(){
		return this.code_;
	}



	public String getTitle(){
		return this.title_;
	}



	public String getURL(){
		return this.url_;
	}



	public String getAuthor(){
		return this.author_;
	}



	public String getKeyword(){
		return this.keyword_;
	}



	public String getUpdate(){
		return this.update_;
	}
}




