package data;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.file.LinkOption;
import java.nio.file.Paths;
import java.util.ArrayList;

public class Sentence {

	private ArrayList<String> sentenceList_;		// 文集合
	private ArrayList<String> typeList_;			// 文集合の種類



/**
 * コンストラクタ
 */
	public Sentence(){
		sentenceList_ = new ArrayList<String>();
		typeList_     = new ArrayList<String>();
	}





/**
 * 文章 s から文情報を生成する.
 *
 * @param s
 * @param key
 */
	public void makeSentence(String s, String key){
	// 会話文の場合は会話文全体を文とする
	// 地の文の場合は key で文章を分割する
		String head = String.valueOf(s.charAt(0));
		String tail = String.valueOf(s.charAt(s.length()-1));
		if ( head.equals("「") && tail.equals("」")){
			sentenceList_.add(s);
			typeList_.add("TALK");
		}else{
			String[] sentences = s.split(key);
			if ( sentences.length > 0 ){
				for ( int i = 0; i < sentences.length-1; i++){
					sentenceList_.add(sentences[i] + key);
					typeList_.add("NARRATIVE");
				}
			// 文末が "」" だった場合の対応
				if ( tail.equals("」") ){
					sentenceList_.add(sentences[sentences.length-1]);
					typeList_.add("NARRATIVE");
				}else{
					sentenceList_.add(sentences[sentences.length-1] + key);
					typeList_.add("NARRATIVE");
				}
			}
		}
	}





/**
 * ファイルから文情報を読み込む.
 *
 * @param fileName
 * @param genre
 */
	public void loadSentence(String fileName, String genre){
		try{
		// 読み込むファイル
			String file = "SENTENCE/" + genre + "/" + fileName;
			FileInputStream   fis = new FileInputStream(file);
			InputStreamReader isr = new InputStreamReader(fis, "SJIS");
			BufferedReader    br  = new BufferedReader(isr);
		// 読み込み
			String line = null;
			while ( (line=br.readLine()) != null){
				String[] data = line.split(",");
				sentenceList_.add(data[0]);
				typeList_.add(data[1]);
			}
			br.close();
			isr.close();
			System.out.println("load sentence : " + fileName);

		} catch ( Exception e){
			e.printStackTrace();
		}
	}





/**
 * ファイルに文情報を書き込む.
 *
 * @param fileName
 * @param genre
 */
	public void saveSentence(String fileName, String genre){
		try{
		// ディレクトリの確認
			String absPath = Paths.get(".").toRealPath(LinkOption.NOFOLLOW_LINKS).toString();
			File directory = new File(absPath + "/SENTENCE/" + genre);
			if (!directory.exists()){
				directory.mkdirs();
			}
		// 書き込むファイル
			String file = directory + "/" + fileName;
			FileOutputStream   fos = new FileOutputStream(file);
			OutputStreamWriter osw = new OutputStreamWriter(fos, "SJIS");
			BufferedWriter     bw  = new BufferedWriter(osw);
		// 書き込み
			for ( int i = 0; i < sentenceList_.size(); i++){
				bw.write(sentenceList_.get(i) + "," + typeList_.get(i));
				bw.newLine();
			}
			bw.close();
			osw.close();
			System.out.println("save sentence : " + fileName);

		} catch ( Exception e){
			e.printStackTrace();
		}
	}





/**
 * 文集合を返す.
 *
 * @return
 */
	public ArrayList<String> getAllSentences(){
		ArrayList<String> ans = new ArrayList<String>();
		for ( int i = 0; i < sentenceList_.size(); i++){
			ans.add(sentenceList_.get(i));
		}

		return ans;
	}





/**
 * 文集合から地の文だけを返す.
 *
 * @return
 */
	public ArrayList<String> getNarrativeSentences(){
		ArrayList<String> ans = new ArrayList<String>();
		for ( int i = 0; i < sentenceList_.size(); i++){
			if ( typeList_.get(i).equals("NARRATIVE")){
				ans.add(sentenceList_.get(i));
			}
		}

		return ans;
	}





/**
 * 文集合から会話文だけを取得する.
 *
 * @return
 */
	public ArrayList<String> getTalkSentences(){
		ArrayList<String> ans = new ArrayList<String>();
		for ( int i = 0; i < sentenceList_.size(); i++){
			if ( typeList_.get(i).equals("TALK")){
				ans.add(sentenceList_.get(i));
			}
		}

		return ans;
	}





/**
 * num 番目の文を返す.
 *
 * @param num
 * @return
 */
	public String getSentence(int num){
		return this.sentenceList_.get(num);
	}





/**
 * 文集合のサイズを返す.
 *
 * @return
 */
	public int getSize(){
		return this.sentenceList_.size();
	}

}







