package data;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.file.LinkOption;
import java.nio.file.Paths;
import java.util.ArrayList;

import morphological_analysis.Mecab;

public class Word {

	private int count_;		// 文数カウンタ

	private ArrayList<String> surfaceList_;		// 単語情報集合 (表層形)
	private ArrayList<String> baseList_;			// 単語情報集合 (基本形)
	private ArrayList<String> posList_;			// 単語情報集合 (品詞)
	private ArrayList<String> readingList_;		// 単語情報集合 (読み)
	private ArrayList<String> pronunList_;			// 単語情報集合 (発音)

	private Mecab mecab_;		// Mecab クラス



/**
 * コンストラクタ
 */
	public Word(){
		count_ = 0;
		surfaceList_ = new ArrayList<String>();
		baseList_    = new ArrayList<String>();
		posList_     = new ArrayList<String>();
		readingList_ = new ArrayList<String>();
		pronunList_  = new ArrayList<String>();
		mecab_       = new Mecab();
	}





/**
 * 文 s から単語情報を生成する.
 *
 * @param s
 */
	public void makeWordInfo(String s){
	// 句読点の処理
		String str = s;
		str = s.replaceAll(",", "、");
		str = s.replaceAll("\\.", "。");
	// 形態素解析
		mecab_.setMecabData(str);
		ArrayList<String> surface = mecab_.getSurface();
		ArrayList<String> base    = mecab_.getLemma();
		ArrayList<String> pos     = mecab_.getPos(true);
		ArrayList<String> pronun  = mecab_.getPronunciation();
		ArrayList<String> reading = mecab_.getReading();
		count_++;
	// 何文目のデータであるかを保存
		String mark = "SENTENCE_NUM:" + count_;
		surfaceList_.add(mark);
		baseList_.add(mark);
		posList_.add(mark);
		pronunList_.add(mark);
		readingList_.add(mark);
	// 単語情報を保存
		for ( int i = 0; i < surface.size(); i++){
			surfaceList_.add(surface.get(i));
			baseList_.add(base.get(i));
			posList_.add(pos.get(i));
			pronunList_.add(pronun.get(i));
			readingList_.add(reading.get(i));
		}

	}





	// 単語情報を読み込む
/**
 * ファイルから単語情報を読み込む
 *
 * @param fileName
 * @param genre
 */
	public void loadWordInfo(String fileName, String genre){
		try{
		// 読み込むファイル
			String file = "WORD/" + genre + "/" + fileName;
			FileInputStream   fis = new FileInputStream(file);
			InputStreamReader isr = new InputStreamReader(fis, "SJIS");
			BufferedReader    br  = new BufferedReader(isr);
		// 読み込み
			String line = null;
			while ( (line=br.readLine()) != null){
				if ( line.contains("SENTENCE_NUM") ){
					surfaceList_.add(line);
					baseList_.add(line);
					posList_.add(line);
					pronunList_.add(line);
					readingList_.add(line);
				}else{
					String[] data = line.split(",");
					surfaceList_.add(data[0]);
					baseList_.add(data[1]);
					posList_.add(data[2]);
					pronunList_.add(data[3]);
					readingList_.add(data[4]);
				}
			}
			br.close();
			isr.close();
			System.out.println("load word : " + fileName);

		} catch ( Exception e){
			e.printStackTrace();
		}
	}





/**
 * ファイルに単語情報を書き込む.
 *
 * @param fileName
 * @param genre
 */
	public void saveWordInfo(String fileName, String genre){
		try{
		// ディレクトリの確認
			String absPath = Paths.get(".").toRealPath(LinkOption.NOFOLLOW_LINKS).toString();
			File directory = new File(absPath + "/WORD/" + genre);
			if (!directory.exists()){
				directory.mkdirs();
			}
		// 書き込むファイル
			String             wFile = directory + "/" + fileName;
			FileOutputStream   fos  = new FileOutputStream(wFile);
			OutputStreamWriter osw  = new OutputStreamWriter(fos, "SJIS");
			BufferedWriter     bw   = new BufferedWriter(osw);
		// 書き込み
			for ( int i = 0; i < surfaceList_.size(); i++){
				if ( surfaceList_.get(i).contains("SENTENCE_NUM")){
					bw.write(surfaceList_.get(i));
				}else{
					bw.write(surfaceList_.get(i) + "," + baseList_.get(i) + "," + posList_.get(i));
					bw.write("," + pronunList_.get(i) + "," + readingList_.get(i));
				}
				bw.newLine();
			}
			bw.close();
			osw.close();
			System.out.println("save word : " + fileName);

		} catch ( Exception e){
			e.printStackTrace();
		}
	}





/**
 * 保持している単語情報を初期化する.
 */
	public void resetWordInfo(){
		surfaceList_ = new ArrayList<String>();
		baseList_    = new ArrayList<String>();
		posList_     = new ArrayList<String>();
		readingList_ = new ArrayList<String>();
		pronunList_  = new ArrayList<String>();
		mecab_       = new Mecab();
		count_ = 0;
	}





/**
 * num 文目の単語情報 (表層形) を取得する.
 *
 * @param num
 * @return
 */
	public ArrayList<String> getWordSurface(int num){
		ArrayList<String> ans = new ArrayList<String>();
		String  start = "SENTENCE_NUM:" + String.valueOf(num);
		String  end   = "SENTENCE_NUM:" + String.valueOf(num+1);
		boolean isGet = false;
		for ( int i = 0; i < surfaceList_.size(); i++){
			String surface = surfaceList_.get(i);
		// n+1 文目のマークを見つけたら取得終了
			if ( surface.equals(end)){
				isGet = false;
				break;
			}
		// 単語情報の取得
			if ( isGet){
				ans.add(new String(surface));
			}
		// n 文目のマークを見つけたら取得開始
			if ( surface.equals(start)){
				isGet = true;
			}
		}

		return ans;
	}





/**
 * num 文目の単語情報 (基本形) を取得する.
 *
 * @param num
 * @return
 */
	public ArrayList<String> getWordBase(int num){
		ArrayList<String> ans = new ArrayList<String>();
		String  start = "SENTENCE_NUM:" + String.valueOf(num);
		String  end   = "SENTENCE_NUM:" + String.valueOf(num+1);
		boolean isGet = false;
		for ( int i = 0; i < baseList_.size(); i++){
			String base = baseList_.get(i);
		// n+1 文目のマークを見つけたら取得終了
			if ( base.equals(end)){
				isGet = false;
				break;
			}
		// 単語情報の取得
			if ( isGet){
				ans.add(new String(base));
			}
		// n 文目のマークを見つけたら取得開始
			if ( base.equals(start)){
				isGet = true;
			}
		}

		return ans;
	}





/**
 * num 文目の単語情報 (品詞) を取得する.
 *
 * @param num
 * @return
 */
	public ArrayList<String> getWordPos(int num){
		ArrayList<String> ans = new ArrayList<String>();
		String  start = "SENTENCE_NUM:" + String.valueOf(num);
		String  end   = "SENTENCE_NUM:" + String.valueOf(num+1);
		boolean isGet = false;
		for ( int i = 0; i < posList_.size(); i++){
			String pos = posList_.get(i);
		// n+1 文目のマークを見つけたら取得終了
			if ( pos.equals(end)){
				isGet = false;
				break;
			}
		// 単語情報の取得
			if ( isGet){
				ans.add(new String(pos));
			}
		// n 文目のマークを見つけたら取得開始
			if ( pos.equals(start)){
				isGet = true;
			}
		}

		return ans;
	}





/**
 * num 文目の単語情報 (発音) を取得する.
 *
 * @param num
 * @return
 */
	public ArrayList<String> getWordPronun(int num){
		ArrayList<String> ans = new ArrayList<String>();
		String  start = "SENTENCE_NUM:" + String.valueOf(num);
		String  end   = "SENTENCE_NUM:" + String.valueOf(num+1);
		boolean isGet = false;
		for ( int i = 0; i < pronunList_.size(); i++){
			String pronun = pronunList_.get(i);
		// n+1 文目のマークを見つけたら取得終了
			if ( pronun.equals(end)){
				isGet = false;
				break;
			}
		// 単語情報の取得
			if ( isGet){
				ans.add(new String(pronun));
			}
		// n 文目のマークを見つけたら取得開始
			if ( pronun.equals(start)){
				isGet = true;
			}
		}

		return ans;
	}





/**
 * num 文目の単語情報 (読み) を取得する.
 *
 * @param num
 * @return
 */
	public ArrayList<String> getWordReading(int num){
		ArrayList<String> ans = new ArrayList<String>();
		String  start = "SENTENCE_NUM:" + String.valueOf(num);
		String  end   = "SENTENCE_NUM:" + String.valueOf(num+1);
		boolean isGet = false;
		for ( int i = 0; i < readingList_.size(); i++){
			String reading = readingList_.get(i);
		// n+1 文目のマークを見つけたら取得終了
			if ( reading.equals(end)){
				isGet = false;
				break;
			}
		// 単語情報の取得
			if ( isGet){
				ans.add(new String(reading));
			}
		// n 文目のマークを見つけたら取得開始
			if ( reading.equals(start)){
				isGet = true;
			}
		}

		return ans;
	}





}





