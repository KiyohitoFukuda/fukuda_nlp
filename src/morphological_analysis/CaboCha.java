package morphological_analysis;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

public class CaboCha {

//形態素解析の結果
	private ArrayList<String> data_;
//戻り値
	private ArrayList<String> output_;
//形態素解析する前の文
	private String str_    = "NO DATA";

	public CaboCha(){
	}


// 構文解析する文をセットする
	public void setCaboChaData(String s){
		try{
			s = String.join("", s.split(" "));

			ProcessBuilder     pb  = new ProcessBuilder(new String[] {"cabocha", "-f1"});
			Process            pro = pb.start();
			OutputStream       os  = pro.getOutputStream();
			OutputStreamWriter osw = new OutputStreamWriter(os, "MS932");
			BufferedWriter     bw  = new BufferedWriter(osw);
			bw.write(s);
			bw.close();
			osw.close();
			os.close();

			InputStream       is    = pro.getInputStream();
			InputStreamReader isr   = new InputStreamReader(is, "MS932");
			BufferedReader    br    = new BufferedReader(isr);
			data_ = new ArrayList<String>();
			String line;
			while ((line=br.readLine()) != null){
				if ( line.equals("EOS")){
					continue;
				}
				data_.add(line);
			}
			str_    = s;

		} catch ( Exception e){
			e.printStackTrace();
		}
	}





// CaboCha を用いて分節を返す. isSurface が true なら表層表現, false なら基本形で返す.
	public ArrayList<String> getSegment(boolean isSurface){
		try{
			output_ = new ArrayList<String>();
			String segment = "";
			int    segNum  = -1;
			int    segSize = 0;
			for ( int i = 0; i < data_.size(); i++){
				if ( data_.get(i).charAt(0) == '*'){
					if ( segNum >= 0 ){
						output_.add(segment);
					}
					segNum++;
					segment = data_.get(i).split(" ")[2];
					segSize = 0;
				}else{
					if ( isSurface ){
						if ( segSize == 0 ){
							segment += data_.get(i).split("\t")[0];
						}else{
							segment += "_" + data_.get(i).split("\t")[0];
						}
					}else{
						if ( segSize == 0 ){
							segment += data_.get(i).split("\t")[1].split(",")[6];
						}else{
							segment += "_" + data_.get(i).split("\t")[1].split(",")[6];
						}
					}
					segSize++;
				}
			}
			output_.add(segment);

			return output_;

		} catch ( Exception e){
			e.printStackTrace();
			return null;
		}
	}







// CaboCha を用いて係り受け関係を返す. isSurface が true なら表層表現, false なら基本形で返す.
	public ArrayList<String> getDependency(boolean isSurface){
		try{
			ArrayList<String> segments = new ArrayList<String>();
			segments = this.getSegment(isSurface);
			String segment = "";
			String seg     = "";
			output_ = new ArrayList<String>();
			for ( int i = 0; i < data_.size(); i++){
				if ( data_.get(i).charAt(0) == '*'){
					int sNum = Integer.parseInt(data_.get(i).split(" ")[1]);
					int dNum = Integer.parseInt(data_.get(i).split(" ")[2].split("D")[0]);
					segment = segments.get(sNum).split("D")[1];
					seg     = segments.get(sNum);
					while ( dNum != -1 ){
//						System.out.print("dNum:" + dNum + "   ");
						sNum    =  dNum;
						segment += ">>" + segments.get(sNum).split("D")[1];
						seg     = segments.get(sNum);
						dNum    =  Integer.parseInt(seg.split("D")[0]);
						output_.add(segment);
					}
					segment = "";
//					System.out.println();
				}
			}

			return output_;

		} catch ( Exception e){
			e.printStackTrace();
			return null;
		}
	}






/**
 * 入力されているデータの文字列を返す.
 * 未入力であれば, "NO DATA" と返す.
 */
	public String toString(){
		return str_;
	}
}







