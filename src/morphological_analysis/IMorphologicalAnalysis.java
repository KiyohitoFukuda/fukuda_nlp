package morphological_analysis;

import java.util.ArrayList;

public interface IMorphologicalAnalysis {

//形態素の原形を返す
	public ArrayList<String> getLemma();

//形態素の表層表現を返す
	public ArrayList<String> getSurface();

//形態素の品詞を返す
	public ArrayList<String> getPos(boolean b);

//形態素の活用の種類を返す
	public ArrayList<String> getConjugatedType();

//形態素の活用形を返す
	public ArrayList<String> getConjugatedForm();

// 形態素の発音を返す
	public ArrayList<String> getPronunciation();

// 形態素の読みを返す
	public ArrayList<String> getReading();

// 分かち書きしたものを返す
	public String getWakati();

//形態素長を返す
	public int getLength();



//データの文字列を返す
	public String toString();
}
