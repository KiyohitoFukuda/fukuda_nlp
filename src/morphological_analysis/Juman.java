package morphological_analysis;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

public class Juman implements IMorphologicalAnalysis{

	//形態素解析の結果
	private ArrayList<String> data_;
	//戻り値
	private ArrayList<String> output_;
	//形態素解析する前の文
	private String str_    = "NO DATA";
	//形態素長
	private int    length_ = 0;

	public Juman(){
	}


	//形態素解析する文をセット
	public void setJumanData(String s){
		try{
			ProcessBuilder     pb  = new ProcessBuilder(new String[] {"juman"});
			Process            p   = pb.start();
			OutputStream       os  = p.getOutputStream();
			OutputStreamWriter osw = new OutputStreamWriter(os, "MS932");
			BufferedWriter     bw  = new BufferedWriter(osw);
			bw.write(s);
			bw.close();
			osw.close();
			os.close();

			InputStream       is    = p.getInputStream();
			InputStreamReader isr   = new InputStreamReader(is, "MS932");
			BufferedReader    br    = new BufferedReader(isr);
			data_ = new ArrayList<String>();
			String line;
			while ((line=br.readLine()) != null){
				if ( line.charAt(0) == '@'){
					continue;
				}
				if ( line.equals("EOS")){
					continue;
				}
				data_.add(line);
			}
			str_    = s;
			length_ = data_.size();

		} catch ( Exception e){
			e.printStackTrace();
		}
	}







// JUMAN を用いて原形を返す
	public ArrayList<String> getLemma() {
		try{
			output_ = new ArrayList<String>();
			for ( int i = 0; i < data_.size(); i++){
				output_.add(data_.get(i).split(" ")[2]);
			}

			return output_;

		} catch ( Exception e){
			e.printStackTrace();
			return null;
		}	}







// JUMAN を用いて表層表現を返す
	public ArrayList<String> getSurface() {
		try{
			output_ = new ArrayList<String>();
			for ( int i = 0; i < data_.size(); i++){
				output_.add(data_.get(i).split(" ")[0]);
			}

			return output_;

		} catch ( Exception e){
			e.printStackTrace();
			return null;
		}
	}







// JUMAN を用いて品詞を返す
	public ArrayList<String> getPos(boolean isDeep) {
		try{
			output_ = new ArrayList<String>();
			for ( int i = 0; i < data_.size(); i++){
				String line = data_.get(i);
				String pos  = "-" + line.split(" ")[3];
				String pos1 = "-" + line.split(" ")[5];

				//詳細な品詞情報を使う場合
				if ( isDeep){
					pos += pos1;
				}
				pos += "-";
				output_.add(pos);
			}

			return output_;

		} catch ( Exception e){
			e.printStackTrace();
			return null;
		}
	}







// JUMAN を用いて活用の種類を返す
	public ArrayList<String> getConjugatedType() {
		try{
			output_ = new ArrayList<String>();
			for ( int i = 0; i < data_.size(); i++){
				data_.add(data_.get(i).split(" ")[7]);
			}

			return output_;

		} catch ( Exception e){
			e.printStackTrace();
			return null;
		}
	}







// JUMAN を用いて活用形を返す
	public ArrayList<String> getConjugatedForm() {
		try{
			output_ = new ArrayList<String>();
			for ( int i = 0; i < data_.size(); i++){
				output_.add(data_.get(i).split(" ")[9]);
			}

			return output_;

		} catch ( Exception e){
			e.printStackTrace();
			return null;
		}
	}







	public ArrayList<String> getCategory(){
		try{
			output_ = new ArrayList<String>();
			for ( int i = 0; i < data_.size(); i++){
				if ( data_.get(i).contains("\"") && data_.get(i).contains("カテゴリ:") ){
					String   info     = data_.get(i).split("\"")[1].split("カテゴリ:")[1].split(" ")[0];
					String[] category = info.split(";");
					output_.add(String.join("_", category));
				}else{
					if ( data_.get(i).split(" ")[5].equals("人名") ){
						output_.add("人名");
					}else if ( data_.get(i).split(" ")[5].equals("地名") ){
						output_.add("地名");
					}else{
						output_.add(" ");
					}
				}
			}

			return output_;

		} catch ( Exception e){
			e.printStackTrace();
			return null;
		}
	}







// セットしたデータの形態素長を返す
	public int getLength(){
		return length_;
	}







	/**
	 * 入力されているデータの文字列を返す.
	 * 未入力であれば, "NO DATA" と返す.
	 */
	public String toString(){
		return str_;
	}


	@Override
	public ArrayList<String> getPronunciation() {
		// TODO 自動生成されたメソッド・スタブ
		return null;
	}


	@Override
	public ArrayList<String> getReading() {
		// TODO 自動生成されたメソッド・スタブ
		return null;
	}

}
