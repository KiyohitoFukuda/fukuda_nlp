package morphological_analysis;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

public class Mecab implements IMorphologicalAnalysis{

	private ArrayList<String> data_;		// 形態素解析の結果
	private ArrayList<String> output_;		// 戻り値
	private String str_    = "NO DATA";		// 形態素解析する前の文
	private int    length_ = 0;				//形態素長


/**
 * コンストラクタ
 */
	public Mecab(){
	}





/**
 * 形態素解析する文をセットする.
 * @param s
 */
	public void setMecabData(String s){
		try{
			s = s.replace(" ", "　");
			s = s.replace("\t", "　");
			s = s.replace(",", "、");
			s = s.replace(".", "。");
//			System.out.println(s);
			ProcessBuilder     pb  = new ProcessBuilder(
					new String[] {"mecab", "-U%M\tunknown,unknown,*,*,*,*,%M,*,*,69\n", "-F%M\t%H,%h\n"});
			Process            pro = pb.start();
			OutputStream       os  = pro.getOutputStream();
			OutputStreamWriter osw = new OutputStreamWriter(os, "MS932");
			BufferedWriter     bw  = new BufferedWriter(osw);
			bw.write(s);
			bw.close();
			osw.close();
			os.close();

			InputStream       is    = pro.getInputStream();
			InputStreamReader isr   = new InputStreamReader(is, "MS932");
			BufferedReader    br    = new BufferedReader(isr);
			data_ = new ArrayList<String>();
			String line;
			while ((line=br.readLine()) != null){
				if ( line.equals("EOS")){
					continue;
				}
				data_.add(line);
			}
			str_    = s;
			length_ = data_.size();

		} catch ( Exception e){
			e.printStackTrace();
		}
	}






/**
 * Mecab を用いて原形を返す.
 * @return
 */
	public ArrayList<String> getLemma(){
		try{
			output_ = new ArrayList<String>();
			for ( int i = 0; i < data_.size(); i++){
				String line = data_.get(i);
				output_.add(line.split(",")[6]);
			}

			return output_;

		} catch ( Exception e){
			e.printStackTrace();
			return null;
		}
	}





/**
 * Mecab を用いて表層表現を返す.
 * @return
 */
	public ArrayList<String> getSurface(){
		try{
			output_ = new ArrayList<String>();
			for ( int i = 0; i < data_.size(); i++){
				String line = data_.get(i);
				output_.add(line.split("\t")[0]);
			}

			return output_;

		} catch ( Exception e){
			e.printStackTrace();
			return null;
		}
	}





/**
 * Mecab を用いて品詞を返す.
 * @return
 */
	public ArrayList<String> getPos(boolean isDeep){
		try{
			output_ = new ArrayList<String>();
			for ( int i = 0; i < data_.size(); i++){
				String line = data_.get(i).split("\t")[1];
				String pos  = "-" + line.split(",")[0];
				String pos1 = "-" + line.split(",")[1];
				String pos2 = "-" + line.split(",")[2];

				//詳細な品詞情報を使う場合
				if ( isDeep){
					pos += pos1 + pos2;
				}
				pos += "-";
				output_.add(pos);
			}

			return output_;

		} catch ( Exception e){
			e.printStackTrace();
			return null;
		}
	}





/**
 * Mecab を用いて品詞 ID を返す.
 * @return
 */
	public ArrayList<String> getPosNumber(){
		try{
			output_ = new ArrayList<String>();
			for ( int i = 0; i < data_.size(); i++ ){
				String posNum = data_.get(i).split(",")[9];
				if ( posNum.equals("*") ){
					posNum = "69";
				}
				output_.add(posNum);
			}

			return output_;

		} catch ( Exception e){
			e.printStackTrace();
			return null;
		}
	}





/**
 * Mecab を用いて活用の種類を返す.
 * @return
 */
	public ArrayList<String> getConjugatedType() {
		try{
			output_ = new ArrayList<String>();
			for ( int i = 0; i < data_.size(); i++){
				String line = data_.get(i);
				output_.add(line.split(",")[4]);
			}

			return output_;

		} catch ( Exception e){
			e.printStackTrace();
			return null;
		}
	}





/**
 * Mecab を用いて活用形を返す.
 * @return
 */
	public ArrayList<String> getConjugatedForm() {
		try{
			output_ = new ArrayList<String>();
			for ( int i = 0; i < data_.size(); i++){
				String line = data_.get(i);
				output_.add(line.split(",")[5]);
			}

			return output_;

		} catch ( Exception e){
			e.printStackTrace();
			return null;
		}
	}





/**
 * Mecab を用いて発音を返す.
 * @return
 */
	public ArrayList<String> getPronunciation(){
		try{
			output_ = new ArrayList<String>();
			for ( int i = 0; i < data_.size(); i++){
				String line = data_.get(i);
				output_.add(line.split(",")[8]);
			}

			return output_;

		} catch ( Exception e){
			e.printStackTrace();
			return null;
		}
	}







/**
 * Mecab を用いて発音を返す.
 * @return
 */
	public ArrayList<String> getReading(){
		try{
			output_ = new ArrayList<String>();
			for ( int i = 0; i < data_.size(); i++){
				String line = data_.get(i);
				output_.add(line.split(",")[7]);
			}

			return output_;
		}

		catch ( Exception e){
			e.printStackTrace();
			return null;
		}
	}





/**
 * Meacb を用いて文を分かち書きする.
 */
	public String getWakati(){
		String wakati = "";

		wakati = data_.get(0).split("\t")[0];
		for ( int i = 1; i < data_.size(); i++ ){
			wakati += " " + data_.get(i).split("\t")[0];
		}

		return wakati;
	}





/**
 * セットしたデータの形態素長を返す.
 * @return
 */
	public int getLength(){
		return length_;
	}





/**
 * 入力されているデータの文字列を返す.
 * 未入力であれば, "NO DATA" と返す.
 * @return
 */
	public String toString(){
		return str_;
	}

}
