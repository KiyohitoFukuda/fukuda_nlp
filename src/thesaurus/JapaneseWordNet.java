package thesaurus;

import java.util.ArrayList;
import java.util.Set;

import edu.cmu.lti.jawjaw.JAWJAW;
import edu.cmu.lti.jawjaw.pobj.POS;
import edu.cmu.lti.lexical_db.ILexicalDatabase;
import edu.cmu.lti.lexical_db.NictWordNet;
import edu.cmu.lti.ws4j.RelatednessCalculator;
import edu.cmu.lti.ws4j.impl.HirstStOnge;
import edu.cmu.lti.ws4j.impl.JiangConrath;
import edu.cmu.lti.ws4j.impl.LeacockChodorow;
import edu.cmu.lti.ws4j.impl.Lesk;
import edu.cmu.lti.ws4j.impl.Lin;
import edu.cmu.lti.ws4j.impl.Path;
import edu.cmu.lti.ws4j.impl.Resnik;
import edu.cmu.lti.ws4j.impl.WuPalmer;
import edu.cmu.lti.ws4j.util.WS4JConfiguration;
import morphological_analysis.Mecab;

public class JapaneseWordNet {

	private Mecab mecab_;		// Mecab クラス

/**
 * コンストラクタ
 */
	public JapaneseWordNet(){
		this.mecab_ = new Mecab();
	}





/**
 * 単語 word に対して指定した品詞の同義語を取得する.
 *
 * @param word
 * @param beforePos
 * @param afterPos
 * @return
 */
	public ArrayList<String> findSynonyms(String word, POS beforePos,  String afterPos){
		try{
			ArrayList<String> ans = new ArrayList<String>();
		// word の同義語を取得
			Set<String> set     = JAWJAW.findSynonyms(word, beforePos);
			String      wordSet = set.toString().split("\\[")[1].split("\\]")[0];
			String[]    words   = wordSet.split(", ");
		// 指定した品詞のものを選択
			for ( int i = 0; i < words.length; i++){
				this.mecab_.setMecabData(words[i]);
				ArrayList<String> pos = this.mecab_.getPos(true);
				if ( pos.get(pos.size()-1).contains(afterPos) ){
					ans.add(words[i]);
				}
			}
			System.out.println("synonyms -> " + ans.size());

			return ans;

		} catch ( Exception e){
			e.printStackTrace();
			return null;
		}
	}





/**
 * 単語 word に対して指定した品詞の上位語を取得する.
 *
 * @param word
 * @param beforePos
 * @param afterPos
 * @return
 */
	public ArrayList<String> findHypernyms(String word, POS beforePos,  String afterPos){
		try{
			ArrayList<String> ans = new ArrayList<String>();
		// word の同義語を取得
			Set<String> set     = JAWJAW.findHypernyms(word, beforePos);
			String      wordSet = set.toString().split("\\[")[1].split("\\]")[0];
			String[]    words   = wordSet.split(", ");
		//指定した品詞のものを選択
			for ( int i = 0; i < words.length; i++){
				this.mecab_.setMecabData(words[i]);
				ArrayList<String> pos = this.mecab_.getPos(true);
				if ( pos.get(pos.size()-1).contains(afterPos) ){
					ans.add(words[i]);
				}
			}
			System.out.println("synonyms -> " + ans.size());

			return ans;

		} catch ( Exception e){
			e.printStackTrace();
			return null;
		}
	}





/**
 * 単語 word に対して指定した品詞の下位語を取得する.
 *
 * @param word
 * @param beforePos
 * @param afterPos
 * @return
 */
	public ArrayList<String> findHyponyms(String word, POS beforePos,  String afterPos){
		try{
			ArrayList<String> ans = new ArrayList<String>();
		// word の下位語を取得
			Set<String> set     = JAWJAW.findHyponyms(word, beforePos);
			String      wordSet = set.toString().split("\\[")[1].split("\\]")[0];
			String[]    words   = wordSet.split(", ");

		//指定した品詞のものを選択
			for ( int i = 0; i < words.length; i++){
				this.mecab_.setMecabData(words[i]);
				ArrayList<String> pos = this.mecab_.getPos(true);
				if ( pos.get(pos.size()-1).contains(afterPos) ){
					ans.add(words[i]);
				}
			}
			System.out.println("synonyms -> " + ans.size());

			return ans;

		} catch ( Exception e){
			e.printStackTrace();
			return null;
		}
	}





	//単語間の類似度を計算する(英語のみ)
/**
 * word1 と word2 の意味的な類似度を計算する (英語のみ)
 * 本当に意味的な類似度を計算できているのかは未確認なので, 使用する際は注意
 * @param word1
 * @param word2
 */
	public void calculateRelatedness(String word1, String word2){
		try{
			WS4JConfiguration.getInstance().setMFS(true);

			ILexicalDatabase db = new NictWordNet();
			RelatednessCalculator[] rcs = {new HirstStOnge(db), new LeacockChodorow(db),
					new Lesk(db), new WuPalmer(db), new Resnik(db), new JiangConrath(db),
					new Lin(db), new Path(db)};

			for ( RelatednessCalculator rc : rcs){
				double relatedness = rc.calcRelatednessOfWords(word1, word2);
				System.out.println(rc.getClass().getName() + "\t:\t" + relatedness);
			}

		} catch ( Exception e){
			e.printStackTrace();
		}
	}

}



