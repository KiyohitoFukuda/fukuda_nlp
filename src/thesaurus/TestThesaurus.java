package thesaurus;

import java.util.ArrayList;

import edu.cmu.lti.jawjaw.pobj.POS;

public class TestThesaurus {

	public static void main(String[] args) {
		JapaneseWordNet jwn = new JapaneseWordNet();
		String s3 = "人間";
		ArrayList<String> l1 = jwn.findSynonyms(s3, POS.n, "名詞-");
		int n1 = 10;
		if ( n1 > l1.size() ){
			n1 = l1.size();
		}
		for ( int i = 0; i < n1; i++ ){
			System.out.println(l1.get(i));
		}

		ArrayList<String> l2 = jwn.findHypernyms(s3, POS.n, "名詞-");
		int n2 = 10;
		if ( n2 > l2.size() ){
			n2 = l2.size();
		}
		for ( int i = 0; i < n2; i++ ){
			System.out.println(l2.get(i));
		}

		ArrayList<String> l3 = jwn.findHyponyms(s3, POS.n, "名詞-");
		int n3 = 10;
		if ( n3 > l3.size() ){
			n3 = l3.size();
		}
		for ( int i = 0; i < n3; i++ ){
			System.out.println(l3.get(i));
		}



		WikipediaThesaurus wt = new WikipediaThesaurus(5);
		ArrayList<String[]> l4 = wt.getRelatedWords("人間");
		for ( int i = 0; i < l4.size(); i++ ){
			String[] s4 = l4.get(i);
			System.out.println(s4[0] + " ---> " + s4[1]);
		}
	}

}
