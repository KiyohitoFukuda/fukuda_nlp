package thesaurus;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.axis2.databinding.types.UnsignedInt;
import org.wikipedia.lab.ServiceStub;
import org.wikipedia.lab.ServiceStub.GetThesaurusDS;
import org.wikipedia.lab.ServiceStub.GetThesaurusDSResponse;
import org.wikipedia.lab.ServiceStub.GetTopCandidateIDFromKeyword;
import org.wikipedia.lab.ServiceStub.GetTopCandidateIDFromKeywordResponse;

//Wikipedia Thesaurus API を用いて関連語,関連度を取得する(上限：30)
public class WikipediaThesaurus {

	private int wordNum_;		// 取得する関連語の上限

	private ServiceStub ss_;


/**
 * コンストラクタ
 *
 * @param wordNumber
 */
	public WikipediaThesaurus(int wordNumber){
		try{
			this.wordNum_ = wordNumber;
			this.ss_ = new ServiceStub();

		} catch ( Exception e){
			e.printStackTrace();
		}
	}










	//keyword の関連語・関連度を取得([関連語, 関連度])
/**
 * keyword の関連語を関連度と共に返す.
 *
 * @param keyword
 * @return
 */
	public ArrayList<String[]> getRelatedWords(String keyword){
		try{
			String[] rWords = fetchRelatedWord(keyword, wordNum_);
			String[] rRates = fetchRelatedRate(keyword, wordNum_);

			ArrayList<String[]> answer = new ArrayList<String[]>();
			for ( int i = 0; i < rWords.length; i++ ){
				String[] ans = new String[2];
				ans[0] = rWords[i];
				ans[1] = rRates[i];
				answer.add(ans);
			}
			return answer;

		} catch ( Exception e){
			e.printStackTrace();
			return null;
		}
	}










	//keyword の関連語を取得
	public String[] getOnlyRwords(String keyword){
		try{
			String[] ans = new String[wordNum_];
			ans = fetchRelatedWord(keyword, wordNum_);

			return ans;

		} catch ( Exception e){
			e.printStackTrace();
			String[] ans = new String[wordNum_];
			for ( int i = 0; i < wordNum_; i++){
				ans[i] = "noWord";
			}
			return ans;
		}
	}









	public int fetchID(String keyword){
		try{
			//戻り値:ans
			int                          ans    = 0;
			GetTopCandidateIDFromKeyword wordID = new GetTopCandidateIDFromKeyword();
			wordID.setKeyword(keyword);
			wordID.setLanguage("Japanese");
			GetTopCandidateIDFromKeywordResponse response = ss_.getTopCandidateIDFromKeyword(wordID);
			ans = response.getGetTopCandidateIDFromKeywordResult().intValue();

			return ans;

		} catch ( Exception e){
			e.printStackTrace();
			return 0;
		}
	}










	public String fetchElements(String word){
		try{
			//戻り値:ans
			String ans = "";
			int id = fetchID(word);
			GetThesaurusDS gtd = new GetThesaurusDS();
			gtd.setLanguage("Japanese");
			gtd.setIFrom(new UnsignedInt(id));
			GetThesaurusDSResponse response = ss_.getThesaurusDS(gtd);
			ans = response.getGetThesaurusDSResult().getExtraElement().toString();

			return ans;

		} catch ( Exception e){
			e.printStackTrace();
			return null;
		}
	}










	public String[] fetchRelatedWord(String keyword, int wordNumber){
		try{
			//戻り値:ans
			String[] ans = new String[wordNumber];
			String element = fetchElements(keyword);
			Pattern pattern = Pattern.compile("<.+?>", Pattern.DOTALL);
			Matcher match = pattern.matcher(element);
			String[] elements = (match.replaceAll(" ")).split(" ");
			for ( int i = 0; i < wordNumber; i++){
				int necessary = (10*i) + 10;
				if ( "".equals(elements[necessary])){
					continue;
				}else{
					ans[i] = elements[necessary];
				}
			}

			return ans;

		} catch ( Exception e){
			e.printStackTrace();
			String[] ans = new String[wordNumber];
			for ( int i = 0; i < wordNum_; i++){
				ans[i] = "noWord";
			}
			return ans;
		}
	}










	public String[] fetchRelatedRate(String keyword, int wordNumber){
		try{
			//戻り値:ans
			String[] ans = new String[wordNumber];
			String element = fetchElements(keyword);
			Pattern pattern = Pattern.compile("<.+?>", Pattern.DOTALL);
			Matcher match = pattern.matcher(element);
			String[] elements = (match.replaceAll(" ")).split(" ");
			for ( int i = 0; i < wordNumber; i++){
				int necessary = (10*i) + 8;
				if ( "".equals(elements[necessary])){
					continue;
				}else{
					ans[i] = elements[necessary];
				}
			}

			return ans;

		} catch ( Exception e){
			e.printStackTrace();
			String[] ans = new String[wordNumber];
			for ( int i = 0; i < wordNum_; i++){
				ans[i] = "0";
			}
			return ans;
		}

	}
}
